Come eseguire il software: 
Nel caso in cui si voglia creare il database da zero viene messo a disposizione uno script denominato AssociazioneSportiva_scrip.sql che provveder� a creare e popolare il database.
Altrimenti viene fornito il database primary data file denominato ASSOCIAZIONE_SPORTIVA.mdf
Cambiare la stringa di connessione al database nei file di configurazione del progetto visual studio, in modo che la stringa di connessione al server comprenda il nome del proprio pc. I file in cui e� necessario apportare le modifiche sono:

- Associazione_sportiva\App.config
- Associazione_sportiva\Settings.settings
- Associazione_sportiva\Settings.Designer.cs
- Associazione_sportiva\AssociazioneSportivaDataClasses.dbml
