﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associazione_sportiva
{
    public partial class Partite : Form
    {
        AssociazioneSportivaDataClassesDataContext db = new AssociazioneSportivaDataClassesDataContext();

        public Partite()
        {
            InitializeComponent();
        }

        private DateTime startDate;
        private DateTime EndDate;

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            startDate = DateTime.Parse(this.dateTimePicker1.Text);
            EndDate = DateTime.Parse(this.dateTimePicker2.Text);
            if (startDate <= EndDate)
            {
                var query = (from part in db.PARTITA
                            where (part.In_casa == '1' &&
                                  part.Punteggio_casa > part.Punteggio_ospite ||
                                  part.In_casa == '0' &&
                                  part.Punteggio_casa < part.Punteggio_ospite) 
                                  && part.Data >= startDate && part.Data <= EndDate 
                            join sq in db.SQUADRA on part.codSquadra equals sq.codSquadra
                            group sq by new {
                                sq.codSquadra,
                                sq.Nome }
                            into g
                            select new { codiceSquadra = g.Key.codSquadra, Nome_Squadra = g.Key.Nome,  Partite_vinte = g.Count() }).OrderByDescending(x => x.Partite_vinte).Take(5);
                dataGridView1.DataSource = query;
            }
            else
            {
                MessageBox.Show("Intervallo temporale non valido");
            }

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
            startDate = DateTime.Parse(this.mese_corrente.Text);

            var q = from s in db.SQUADRA
                    join p in db.PARTITA on s.codSquadra equals p.codSquadra
                    where p.Data.Month == startDate.Month 
                    orderby s.Nome
                    select new { s.Nome, p.Data, p.Squadra_avversaria, p.Punteggio_casa, p.Punteggio_ospite };

            if (q.Count() == 0)
            {
                MessageBox.Show("Non sono presenti partite in quel mese");
            }

            else
            {
                dataGridView1.DataSource = q;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var query = from sq in db.SQUADRA
                        where sq.codSquadra == IDSquadra.Text
                        select sq;
            if (query.Count() == 0)
            {
                MessageBox.Show("Codice squadra errato");
                IDSquadra.ResetText();
            }
            else
            {
                var q = from allenamento in db.ALLENAMENTO
                        join imp in db.IMPIANTO on new { allenamento.codImpianto, allenamento.num_campo } equals new { imp.codImpianto, imp.num_campo } into luogo
                        join p in db.prevede on new { allenamento.Giorno, allenamento.Orario_inizio, allenamento.codImpianto, allenamento.num_campo }
                            equals new { p.Giorno, p.Orario_inizio, p.codImpianto, p.num_campo } into all
                        from l in luogo
                        from a in all
                        where a.codSquadra == IDSquadra.Text
                        select new { a.codSquadra, allenamento.Giorno, allenamento.Orario_inizio, allenamento.Orario_fine, Nome_Impianto = l.Nome, l.num_campo, l.Indirizzo };
                dataGridView1.DataSource = q;
            }
        }
    }
}
