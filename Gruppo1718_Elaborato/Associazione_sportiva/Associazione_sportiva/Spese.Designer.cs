﻿namespace Associazione_sportiva
{
    partial class Spese
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DataFattura = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cf_socio = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cod_banca = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.data_pagamento = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numero_transizione = new System.Windows.Forms.TextBox();
            this.modalita = new System.Windows.Forms.TextBox();
            this.num_fatt_pag = new System.Windows.Forms.TextBox();
            this.cod_materiale = new System.Windows.Forms.TextBox();
            this.desc_materiale = new System.Windows.Forms.TextBox();
            this.quantita = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.data_acquisto = new System.Windows.Forms.DateTimePicker();
            this.p_iva_forn = new System.Windows.Forms.TextBox();
            this.num_fattura = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.importo = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.desc_acquisto = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DataFattura);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.cf_socio);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.cod_banca);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.data_pagamento);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.numero_transizione);
            this.groupBox2.Controls.Add(this.modalita);
            this.groupBox2.Controls.Add(this.num_fatt_pag);
            this.groupBox2.Location = new System.Drawing.Point(582, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 572);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PAGAMENTO ACQUISTO MATERIALE";
            // 
            // DataFattura
            // 
            this.DataFattura.Location = new System.Drawing.Point(190, 72);
            this.DataFattura.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DataFattura.Name = "DataFattura";
            this.DataFattura.Size = new System.Drawing.Size(298, 26);
            this.DataFattura.TabIndex = 37;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 85);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 20);
            this.label15.TabIndex = 36;
            this.label15.Text = "Data Fattura";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(190, 471);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 71);
            this.button2.TabIndex = 35;
            this.button2.Text = "INSERISCI PAGAMENTO SPESA";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 322);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 20);
            this.label14.TabIndex = 34;
            this.label14.Text = "CF socio";
            // 
            // cf_socio
            // 
            this.cf_socio.Location = new System.Drawing.Point(190, 318);
            this.cf_socio.Name = "cf_socio";
            this.cf_socio.Size = new System.Drawing.Size(308, 26);
            this.cf_socio.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 275);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 20);
            this.label13.TabIndex = 32;
            this.label13.Text = "Codice Banca";
            // 
            // cod_banca
            // 
            this.cod_banca.Location = new System.Drawing.Point(190, 272);
            this.cod_banca.Name = "cod_banca";
            this.cod_banca.Size = new System.Drawing.Size(308, 26);
            this.cod_banca.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 229);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 20);
            this.label12.TabIndex = 30;
            this.label12.Text = "Modalità";
            // 
            // data_pagamento
            // 
            this.data_pagamento.Location = new System.Drawing.Point(190, 128);
            this.data_pagamento.Name = "data_pagamento";
            this.data_pagamento.Size = new System.Drawing.Size(308, 26);
            this.data_pagamento.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(146, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "Numero transizione";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "Data pagamento";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Numero fattura";
            // 
            // numero_transizione
            // 
            this.numero_transizione.Location = new System.Drawing.Point(190, 178);
            this.numero_transizione.Name = "numero_transizione";
            this.numero_transizione.Size = new System.Drawing.Size(308, 26);
            this.numero_transizione.TabIndex = 18;
            // 
            // modalita
            // 
            this.modalita.Location = new System.Drawing.Point(190, 226);
            this.modalita.Name = "modalita";
            this.modalita.Size = new System.Drawing.Size(308, 26);
            this.modalita.TabIndex = 17;
            // 
            // num_fatt_pag
            // 
            this.num_fatt_pag.Location = new System.Drawing.Point(190, 32);
            this.num_fatt_pag.Name = "num_fatt_pag";
            this.num_fatt_pag.Size = new System.Drawing.Size(308, 26);
            this.num_fatt_pag.TabIndex = 16;
            // 
            // cod_materiale
            // 
            this.cod_materiale.Location = new System.Drawing.Point(184, 80);
            this.cod_materiale.Name = "cod_materiale";
            this.cod_materiale.Size = new System.Drawing.Size(308, 26);
            this.cod_materiale.TabIndex = 15;
            // 
            // desc_materiale
            // 
            this.desc_materiale.Location = new System.Drawing.Point(184, 128);
            this.desc_materiale.Name = "desc_materiale";
            this.desc_materiale.Size = new System.Drawing.Size(308, 26);
            this.desc_materiale.TabIndex = 18;
            // 
            // quantita
            // 
            this.quantita.Location = new System.Drawing.Point(184, 278);
            this.quantita.Name = "quantita";
            this.quantita.Size = new System.Drawing.Size(308, 26);
            this.quantita.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-28, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "CF";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Data acquisto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 282);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 23;
            this.label3.Text = "Quantità";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 335);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 20);
            this.label4.TabIndex = 24;
            this.label4.Text = "Partita IVA fornitore";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "Descrizione materiale";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 20);
            this.label7.TabIndex = 27;
            this.label7.Text = "codice materiale";
            // 
            // data_acquisto
            // 
            this.data_acquisto.Location = new System.Drawing.Point(183, 228);
            this.data_acquisto.Name = "data_acquisto";
            this.data_acquisto.Size = new System.Drawing.Size(308, 26);
            this.data_acquisto.TabIndex = 28;
            // 
            // p_iva_forn
            // 
            this.p_iva_forn.Location = new System.Drawing.Point(184, 335);
            this.p_iva_forn.Name = "p_iva_forn";
            this.p_iva_forn.Size = new System.Drawing.Size(308, 26);
            this.p_iva_forn.TabIndex = 30;
            // 
            // num_fattura
            // 
            this.num_fattura.Location = new System.Drawing.Point(183, 175);
            this.num_fattura.Name = "num_fattura";
            this.num_fattura.Size = new System.Drawing.Size(308, 26);
            this.num_fattura.TabIndex = 31;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(184, 495);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 71);
            this.button1.TabIndex = 32;
            this.button1.Text = "INSERISCI SPESA\r\nACQUISTO MATERIALE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 20);
            this.label8.TabIndex = 33;
            this.label8.Text = "Numero fattura";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 389);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 20);
            this.label6.TabIndex = 34;
            this.label6.Text = "Importo totale";
            // 
            // importo
            // 
            this.importo.Location = new System.Drawing.Point(183, 389);
            this.importo.Name = "importo";
            this.importo.Size = new System.Drawing.Size(308, 26);
            this.importo.TabIndex = 35;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.desc_acquisto);
            this.groupBox1.Controls.Add(this.importo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.num_fattura);
            this.groupBox1.Controls.Add(this.p_iva_forn);
            this.groupBox1.Controls.Add(this.data_acquisto);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.quantita);
            this.groupBox1.Controls.Add(this.desc_materiale);
            this.groupBox1.Controls.Add(this.cod_materiale);
            this.groupBox1.Location = new System.Drawing.Point(32, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 572);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ACQUISTO MATERIALE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 451);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(156, 20);
            this.label16.TabIndex = 37;
            this.label16.Text = "Descrizione acquisto";
            // 
            // desc_acquisto
            // 
            this.desc_acquisto.Location = new System.Drawing.Point(184, 446);
            this.desc_acquisto.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.desc_acquisto.Name = "desc_acquisto";
            this.desc_acquisto.Size = new System.Drawing.Size(308, 26);
            this.desc_acquisto.TabIndex = 36;
            // 
            // Spese
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 662);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Spese";
            this.Text = "Spese";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox cf_socio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox cod_banca;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker data_pagamento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox numero_transizione;
        private System.Windows.Forms.TextBox modalita;
        private System.Windows.Forms.TextBox num_fatt_pag;
        private System.Windows.Forms.DateTimePicker DataFattura;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox cod_materiale;
        private System.Windows.Forms.TextBox desc_materiale;
        private System.Windows.Forms.TextBox quantita;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker data_acquisto;
        private System.Windows.Forms.TextBox p_iva_forn;
        private System.Windows.Forms.TextBox num_fattura;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox importo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox desc_acquisto;
    }
}