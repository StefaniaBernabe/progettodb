﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associazione_sportiva
{

    public partial class Entrate : Form
    {
        AssociazioneSportivaDataClassesDataContext db = new AssociazioneSportivaDataClassesDataContext();

        public Entrate()
        {
            InitializeComponent();
            string QS_numFattura = qsNumFattura.Text;
        }


        private void button1_Click(object sender, EventArgs e)
        {
                QUOTA_SOCIALE quota_sociale = new QUOTA_SOCIALE();
                quota_sociale.Data = DateTime.Parse(dataQuotaSociale.Text);
                quota_sociale.numFattura = this.qsNumFattura.Text;
                quota_sociale.Importo = float.Parse(this.importoQS.Text);
                quota_sociale.Descrizione = this.descrQS.Text;
                quota_sociale.CF = this.textBox1.Text;
            var query = from qs in db.QUOTA_SOCIALE
                            where qs.Data == quota_sociale.Data && qs.numFattura == quota_sociale.numFattura
                            select qs;
            var queryCF = from socio in db.SOCIO
                          where socio.CF == quota_sociale.CF
                          select socio;
    
                if(query.Count() == 1 || queryCF.Count() != 1)
                {
                System.Windows.Forms.MessageBox.Show("Impossibile inserire la QUOTA SOCIALE, chiave gia presente o CF non corretto");

                }
            else
            {
                db.QUOTA_SOCIALE.InsertOnSubmit(quota_sociale);
                db.SubmitChanges();
                MessageBox.Show("Inserimento riuscito");
            }    
        }

        private void button2_Click(object sender, EventArgs e)
        {
            QUOTA_ANNUALE_TESSERATO quota = new QUOTA_ANNUALE_TESSERATO();
            quota.numFattura = this.QTnumFattura.Text;
            quota.Data = DateTime.Parse(this.QTdata.Text);
            quota.Importo = float.Parse(this.QTimporto.Text);
            quota.Descrizione = this.QTdescr.Text;
            quota.CF = this.QTcf.Text;

            var query = from qt in db.QUOTA_ANNUALE_TESSERATO
                        where qt.numFattura == quota.numFattura && qt.Data == quota.Data
                        select qt;
            var queryCF = from tesserato in db.TESSERATO
                          where tesserato.CF == quota.CF
                          select tesserato;
            if(query.Count() == 1 || queryCF.Count() !=1)
            {
                System.Windows.Forms.MessageBox.Show("Impossibile inserire la QUOTA ANNUALE, chiave gia presente o CF non corretto");
            }
            else
            {
                db.QUOTA_ANNUALE_TESSERATO.InsertOnSubmit(quota);
                db.SubmitChanges();
                MessageBox.Show("Inserimento riuscito");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SPONSORIZZAZIONE sponsorizzazione = new SPONSORIZZAZIONE();
            sponsorizzazione.numFattura = this.SPnumFattura.Text;
            sponsorizzazione.Data = DateTime.Parse(this.SPData.Text);
            sponsorizzazione.Importo = float.Parse(this.SPImporto.Text);
            sponsorizzazione.Descrizione = this.SPdescr.Text;
            sponsorizzazione.P_IVA = this.SPp_iva.Text;

            var query = from spons in db.SPONSORIZZAZIONE
                        where spons.numFattura == sponsorizzazione.numFattura && spons.Data == sponsorizzazione.Data
                        select spons;
            var queryP_IVA = from sp in db.SPONSOR
                             where sp.P_IVA == sponsorizzazione.P_IVA
                             select sp;
            if (query.Count() == 1 || queryP_IVA.Count() != 1)
            {
                System.Windows.Forms.MessageBox.Show("Impossibile inserire la SPONSORIZZAZIONE, chiave gia presente o P_IVA non corretta");
            }
            else
            {
                db.SPONSORIZZAZIONE.InsertOnSubmit(sponsorizzazione);
                db.SubmitChanges();
                MessageBox.Show("Inserimento riuscito");
            }
        }

        private void QTdata_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            int anno = int.Parse(this.Anno.Text);
            var query = from tesserato in db.TESSERATO
                        where tesserato.ScadenzaTesseramento.Year != anno
                        select tesserato;
            this.dataGridView1.DataSource = query;
        }
    }
}
