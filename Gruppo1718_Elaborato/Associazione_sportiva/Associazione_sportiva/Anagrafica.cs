﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associazione_sportiva
{
    public partial class Anagrafica : Form
    {
        AssociazioneSportivaDataClassesDataContext db = new AssociazioneSportivaDataClassesDataContext();

        public Anagrafica()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SOCIO socio = new SOCIO();
            socio.CF = this.CF.Text;
            socio.Nome = this.Nome.Text;
            socio.Cognome = this.Cognome.Text;
            socio.Indirizzo = this.Indirizzo.Text;
            socio.Telefono = decimal.Parse(this.Telefono.Text);
            socio.Data_di_nascita = DateTime.Parse(this.data_di_nascita.Text);
            socio.Data_annessione = DateTime.Parse(this.data_di_annessione.Text);

            var checkCF = from s in db.SOCIO
                          where s.CF == socio.CF
                          select s;

            if (checkCF.Count() != 0)
            {
                MessageBox.Show("Impossibile inserire il SOCIO, chiave gia presente");
            }
            else
            {
                MessageBox.Show("Inserimento riuscito");
                db.SOCIO.InsertOnSubmit(socio);
                db.SubmitChanges();
            }

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            TESSERATO tesserato = new TESSERATO();
            tesserato.CF = this.CF_t.Text;
            tesserato.Nome = this.Nome_t.Text;
            tesserato.Cognome = this.Cognome_t.Text;
            tesserato.Indirizzo = this.Indirizzo_t.Text;
            tesserato.Telefono = decimal.Parse(this.Telefono_t.Text);
            tesserato.Data_di_nascita = DateTime.Parse(this.data_di_nascita_t.Text);
            tesserato.ScadenzaTesseramento = this.data_scadenza.Value;
            tesserato.Qualifica = this.Qualifica.Text;

            var checkCF_T = from t in db.TESSERATO
                          where t.CF == tesserato.CF
                          select t;

            if (checkCF_T.Count() !=  0)
            {
                MessageBox.Show("Impossibile inserire il TESSERATO, chiave gia presente");
            }
            else
            {
                MessageBox.Show("Inserimento riuscito");
                db.TESSERATO.InsertOnSubmit(tesserato);
                db.SubmitChanges();
            }

        }
    }
}
