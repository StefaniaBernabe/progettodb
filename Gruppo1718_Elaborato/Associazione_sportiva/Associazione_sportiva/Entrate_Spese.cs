﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associazione_sportiva
{
    public partial class Entrate_Spese : Form
    {
        public Entrate_Spese()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Spese showSpese = new Spese();
            showSpese.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Entrate showEntrate = new Entrate();
            showEntrate.Show();
        }
    }
}
