﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associazione_sportiva
{
    public partial class Spese : Form
    {
        AssociazioneSportivaDataClassesDataContext db = new AssociazioneSportivaDataClassesDataContext();

        public Spese()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ACQUISTO_MATERIALE acquisto = new ACQUISTO_MATERIALE();
            acquisto.numFattura = this.num_fattura.Text;
            acquisto.Data = this.data_acquisto.Value;
            acquisto.Importo = float.Parse(this.importo.Text);
            acquisto.Descrizione = this.desc_acquisto.Text;
            acquisto.Partita_Iva = this.p_iva_forn.Text;

            var chek_forn = from f in db.FORNITORE
                            where f.Partita_Iva == acquisto.Partita_Iva
                            select f;

            if (chek_forn.Count() == 0)
            {
                MessageBox.Show("fornitore non presente nell'elenco dei fornitori");
            }
            else
            {
                var cerca_materiale = from m in db.MATERIALE
                                      where m.codMateriale == this.cod_materiale.Text
                                      select m;

                if (cerca_materiale.Count() == 0)
                {
                    MATERIALE mat = new MATERIALE();
                    mat.codMateriale = this.cod_materiale.Text;
                    mat.Descrizione = this.desc_materiale.Text;
                    mat.Giacenza = float.Parse(this.quantita.Text);

                    db.MATERIALE.InsertOnSubmit(mat);
                    db.SubmitChanges();
                }
                else
                {
                    cerca_materiale.First().Giacenza = cerca_materiale.First().Giacenza + float.Parse(this.quantita.Text);
                }

                db.ACQUISTO_MATERIALE.InsertOnSubmit(acquisto);
                db.SubmitChanges();

                acquisto_beni acq_beni = new acquisto_beni();
                acq_beni.codMateriale = this.cod_materiale.Text;
                acq_beni.Data = this.data_acquisto.Value;
                acq_beni.numFattura = this.num_fattura.Text;
                acq_beni.QuantitÃ = float.Parse(this.quantita.Text);

                db.acquisto_beni.InsertOnSubmit(acq_beni);
                db.SubmitChanges();
                MessageBox.Show("Inserimento riuscito");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PAGAMENTO pagamento = new PAGAMENTO();
            pagamento.Data = this.data_pagamento.Value;
            pagamento.NumTransazione = this.numero_transizione.Text;
            pagamento.ModalitÃ = this.modalita.Text;
            pagamento.CodiceBanca = this.cod_banca.Text;
            pagamento.CF = this.cf_socio.Text;

            var check_pagamento = from p in db.PAGAMENTO
                                  where p.Data == this.data_pagamento.Value && p.NumTransazione == this.numero_transizione.Text
                                  select p;
            if (check_pagamento.Count() == 0)
            {
                var check_socio = from s in db.SOCIO
                                  where s.CF == this.cf_socio.Text
                                  select s;

                var check_numFatt = from a in db.ACQUISTO_MATERIALE
                                    where a.numFattura == this.num_fatt_pag.Text && a.Data == this.DataFattura.Value
                                    select a;

                if (check_socio.Count() == 0)
                {
                    MessageBox.Show("socio non presente");
                }
                else
                {
                    if (check_numFatt.Count() == 0)
                    {
                        MessageBox.Show("Fattura acquisto materiale non presente");
                    }
                    else
                    {
                        db.PAGAMENTO.InsertOnSubmit(pagamento);
                        db.SubmitChanges();

                        var search_acq = (from a in db.ACQUISTO_MATERIALE
                                          where a.numFattura == this.num_fatt_pag.Text && a.Data.Equals(this.DataFattura.Value)
                                          select a).First();
                        search_acq.Pag_Data = this.data_pagamento.Value;
                        search_acq.Pag_NumTransazione = this.numero_transizione.Text;
                        db.SubmitChanges();
                        MessageBox.Show("Inserimento riuscito");
                    }
                }
            }
            else { MessageBox.Show("Data e id transazione gia presenti"); }
        }
    }
}
