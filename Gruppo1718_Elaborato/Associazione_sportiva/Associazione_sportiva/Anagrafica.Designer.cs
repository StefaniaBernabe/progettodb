﻿namespace Associazione_sportiva
{
    partial class Anagrafica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.data_di_annessione = new System.Windows.Forms.DateTimePicker();
            this.data_di_nascita = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.Telefono = new System.Windows.Forms.TextBox();
            this.Indirizzo = new System.Windows.Forms.TextBox();
            this.Cognome = new System.Windows.Forms.TextBox();
            this.Nome = new System.Windows.Forms.TextBox();
            this.CF = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.data_di_nascita_t = new System.Windows.Forms.DateTimePicker();
            this.data_scadenza = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.Qualifica = new System.Windows.Forms.ComboBox();
            this.Telefono_t = new System.Windows.Forms.TextBox();
            this.Indirizzo_t = new System.Windows.Forms.TextBox();
            this.Cognome_t = new System.Windows.Forms.TextBox();
            this.Nome_t = new System.Windows.Forms.TextBox();
            this.CF_t = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.data_di_annessione);
            this.groupBox1.Controls.Add(this.data_di_nascita);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.Telefono);
            this.groupBox1.Controls.Add(this.Indirizzo);
            this.groupBox1.Controls.Add(this.Cognome);
            this.groupBox1.Controls.Add(this.Nome);
            this.groupBox1.Controls.Add(this.CF);
            this.groupBox1.Location = new System.Drawing.Point(8, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(343, 360);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SOCIO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 232);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Data di annessione";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 201);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Data di nascita";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 169);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Telefono";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Indirizzo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 99);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Cognome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 66);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "CF";
            // 
            // data_di_annessione
            // 
            this.data_di_annessione.Location = new System.Drawing.Point(116, 229);
            this.data_di_annessione.Margin = new System.Windows.Forms.Padding(2);
            this.data_di_annessione.Name = "data_di_annessione";
            this.data_di_annessione.Size = new System.Drawing.Size(207, 20);
            this.data_di_annessione.TabIndex = 8;
            // 
            // data_di_nascita
            // 
            this.data_di_nascita.Location = new System.Drawing.Point(116, 197);
            this.data_di_nascita.Margin = new System.Windows.Forms.Padding(2);
            this.data_di_nascita.Name = "data_di_nascita";
            this.data_di_nascita.Size = new System.Drawing.Size(207, 20);
            this.data_di_nascita.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(90, 313);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "INSERISCI SOCIO";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Telefono
            // 
            this.Telefono.Location = new System.Drawing.Point(116, 165);
            this.Telefono.Margin = new System.Windows.Forms.Padding(2);
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(207, 20);
            this.Telefono.TabIndex = 4;
            // 
            // Indirizzo
            // 
            this.Indirizzo.Location = new System.Drawing.Point(116, 134);
            this.Indirizzo.Margin = new System.Windows.Forms.Padding(2);
            this.Indirizzo.Name = "Indirizzo";
            this.Indirizzo.Size = new System.Drawing.Size(207, 20);
            this.Indirizzo.TabIndex = 3;
            // 
            // Cognome
            // 
            this.Cognome.Location = new System.Drawing.Point(116, 99);
            this.Cognome.Margin = new System.Windows.Forms.Padding(2);
            this.Cognome.Name = "Cognome";
            this.Cognome.Size = new System.Drawing.Size(207, 20);
            this.Cognome.TabIndex = 2;
            // 
            // Nome
            // 
            this.Nome.Location = new System.Drawing.Point(116, 62);
            this.Nome.Margin = new System.Windows.Forms.Padding(2);
            this.Nome.Name = "Nome";
            this.Nome.Size = new System.Drawing.Size(207, 20);
            this.Nome.TabIndex = 1;
            // 
            // CF
            // 
            this.CF.Location = new System.Drawing.Point(116, 29);
            this.CF.Margin = new System.Windows.Forms.Padding(2);
            this.CF.Name = "CF";
            this.CF.Size = new System.Drawing.Size(207, 20);
            this.CF.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.data_di_nascita_t);
            this.groupBox2.Controls.Add(this.data_scadenza);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.Qualifica);
            this.groupBox2.Controls.Add(this.Telefono_t);
            this.groupBox2.Controls.Add(this.Indirizzo_t);
            this.groupBox2.Controls.Add(this.Cognome_t);
            this.groupBox2.Controls.Add(this.Nome_t);
            this.groupBox2.Controls.Add(this.CF_t);
            this.groupBox2.Location = new System.Drawing.Point(401, 25);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(336, 360);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TESSERATO";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 264);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "Scadenza idoneità";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 229);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Qualifica";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Data di nascita";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 166);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Telefono";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 131);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Indirizzo";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 96);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Cognome";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 62);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Nome";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 29);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "CF";
            // 
            // data_di_nascita_t
            // 
            this.data_di_nascita_t.Location = new System.Drawing.Point(115, 197);
            this.data_di_nascita_t.Margin = new System.Windows.Forms.Padding(2);
            this.data_di_nascita_t.Name = "data_di_nascita_t";
            this.data_di_nascita_t.Size = new System.Drawing.Size(207, 20);
            this.data_di_nascita_t.TabIndex = 15;
            // 
            // data_scadenza
            // 
            this.data_scadenza.Location = new System.Drawing.Point(115, 264);
            this.data_scadenza.Margin = new System.Windows.Forms.Padding(2);
            this.data_scadenza.Name = "data_scadenza";
            this.data_scadenza.Size = new System.Drawing.Size(207, 20);
            this.data_scadenza.TabIndex = 14;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(115, 313);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 27);
            this.button2.TabIndex = 13;
            this.button2.Text = "INSERISCI TESSERATO";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Qualifica
            // 
            this.Qualifica.AutoCompleteCustomSource.AddRange(new string[] {
            "ATLETA",
            "COLLABORATORE",
            "TECNICO"});
            this.Qualifica.FormattingEnabled = true;
            this.Qualifica.Items.AddRange(new object[] {
            "ATLETA",
            "TECNICO",
            "COLLABORATORE"});
            this.Qualifica.Location = new System.Drawing.Point(115, 230);
            this.Qualifica.Margin = new System.Windows.Forms.Padding(2);
            this.Qualifica.Name = "Qualifica";
            this.Qualifica.Size = new System.Drawing.Size(206, 21);
            this.Qualifica.TabIndex = 12;
            // 
            // Telefono_t
            // 
            this.Telefono_t.Location = new System.Drawing.Point(115, 165);
            this.Telefono_t.Margin = new System.Windows.Forms.Padding(2);
            this.Telefono_t.Name = "Telefono_t";
            this.Telefono_t.Size = new System.Drawing.Size(207, 20);
            this.Telefono_t.TabIndex = 10;
            // 
            // Indirizzo_t
            // 
            this.Indirizzo_t.Location = new System.Drawing.Point(115, 134);
            this.Indirizzo_t.Margin = new System.Windows.Forms.Padding(2);
            this.Indirizzo_t.Name = "Indirizzo_t";
            this.Indirizzo_t.Size = new System.Drawing.Size(207, 20);
            this.Indirizzo_t.TabIndex = 9;
            // 
            // Cognome_t
            // 
            this.Cognome_t.Location = new System.Drawing.Point(115, 99);
            this.Cognome_t.Margin = new System.Windows.Forms.Padding(2);
            this.Cognome_t.Name = "Cognome_t";
            this.Cognome_t.Size = new System.Drawing.Size(207, 20);
            this.Cognome_t.TabIndex = 8;
            // 
            // Nome_t
            // 
            this.Nome_t.Location = new System.Drawing.Point(115, 62);
            this.Nome_t.Margin = new System.Windows.Forms.Padding(2);
            this.Nome_t.Name = "Nome_t";
            this.Nome_t.Size = new System.Drawing.Size(207, 20);
            this.Nome_t.TabIndex = 7;
            // 
            // CF_t
            // 
            this.CF_t.Location = new System.Drawing.Point(115, 29);
            this.CF_t.Margin = new System.Windows.Forms.Padding(2);
            this.CF_t.Name = "CF_t";
            this.CF_t.Size = new System.Drawing.Size(207, 20);
            this.CF_t.TabIndex = 6;
            // 
            // Anagrafica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Anagrafica";
            this.Text = "Anagrafica";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Telefono;
        private System.Windows.Forms.TextBox Indirizzo;
        private System.Windows.Forms.TextBox Cognome;
        private System.Windows.Forms.TextBox Nome;
        private System.Windows.Forms.TextBox CF;
        private System.Windows.Forms.ComboBox Qualifica;
        private System.Windows.Forms.TextBox Telefono_t;
        private System.Windows.Forms.TextBox Indirizzo_t;
        private System.Windows.Forms.TextBox Cognome_t;
        private System.Windows.Forms.TextBox Nome_t;
        private System.Windows.Forms.TextBox CF_t;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker data_di_annessione;
        private System.Windows.Forms.DateTimePicker data_di_nascita;
        private System.Windows.Forms.DateTimePicker data_scadenza;
        private System.Windows.Forms.DateTimePicker data_di_nascita_t;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
    }
}