﻿namespace Associazione_sportiva
{
    partial class Entrate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataQuotaSociale = new System.Windows.Forms.DateTimePicker();
            this.descrQS = new System.Windows.Forms.TextBox();
            this.importoQS = new System.Windows.Forms.TextBox();
            this.qsNumFattura = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.QTInsert = new System.Windows.Forms.Button();
            this.QTcf = new System.Windows.Forms.TextBox();
            this.QTdata = new System.Windows.Forms.DateTimePicker();
            this.QTdescr = new System.Windows.Forms.TextBox();
            this.QTimporto = new System.Windows.Forms.TextBox();
            this.QTnumFattura = new System.Windows.Forms.TextBox();
            this.SPnumFattura = new System.Windows.Forms.TextBox();
            this.SPImporto = new System.Windows.Forms.TextBox();
            this.SPdescr = new System.Windows.Forms.TextBox();
            this.SPData = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SPp_iva = new System.Windows.Forms.TextBox();
            this.SPinsert = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Anno = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.dataQuotaSociale);
            this.groupBox1.Controls.Add(this.descrQS);
            this.groupBox1.Controls.Add(this.importoQS);
            this.groupBox1.Controls.Add(this.qsNumFattura);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 295);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "QUOTA SOCIALE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(37, 200);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "CF socio";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Data";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(37, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Importo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Descrizione";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Numero Fattura";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(37, 221);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(175, 20);
            this.textBox1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(82, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "INSERISCI";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataQuotaSociale
            // 
            this.dataQuotaSociale.Location = new System.Drawing.Point(37, 85);
            this.dataQuotaSociale.Name = "dataQuotaSociale";
            this.dataQuotaSociale.Size = new System.Drawing.Size(175, 20);
            this.dataQuotaSociale.TabIndex = 6;
            // 
            // descrQS
            // 
            this.descrQS.Location = new System.Drawing.Point(37, 174);
            this.descrQS.Multiline = true;
            this.descrQS.Name = "descrQS";
            this.descrQS.Size = new System.Drawing.Size(175, 20);
            this.descrQS.TabIndex = 3;
            // 
            // importoQS
            // 
            this.importoQS.Location = new System.Drawing.Point(37, 128);
            this.importoQS.Name = "importoQS";
            this.importoQS.Size = new System.Drawing.Size(175, 20);
            this.importoQS.TabIndex = 2;
            // 
            // qsNumFattura
            // 
            this.qsNumFattura.Location = new System.Drawing.Point(37, 36);
            this.qsNumFattura.Multiline = true;
            this.qsNumFattura.Name = "qsNumFattura";
            this.qsNumFattura.Size = new System.Drawing.Size(175, 20);
            this.qsNumFattura.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.QTInsert);
            this.groupBox2.Controls.Add(this.QTcf);
            this.groupBox2.Controls.Add(this.QTdata);
            this.groupBox2.Controls.Add(this.QTdescr);
            this.groupBox2.Controls.Add(this.QTimporto);
            this.groupBox2.Controls.Add(this.QTnumFattura);
            this.groupBox2.Location = new System.Drawing.Point(281, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 295);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "QUOTA ANNUALE TESSERAMENTO";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "CF tesserato";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Descrizione";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Importo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Numero Fattura";
            // 
            // QTInsert
            // 
            this.QTInsert.Location = new System.Drawing.Point(91, 257);
            this.QTInsert.Name = "QTInsert";
            this.QTInsert.Size = new System.Drawing.Size(75, 23);
            this.QTInsert.TabIndex = 12;
            this.QTInsert.Text = "INSERISCI";
            this.QTInsert.UseVisualStyleBackColor = true;
            this.QTInsert.Click += new System.EventHandler(this.button2_Click);
            // 
            // QTcf
            // 
            this.QTcf.Location = new System.Drawing.Point(43, 221);
            this.QTcf.Name = "QTcf";
            this.QTcf.Size = new System.Drawing.Size(177, 20);
            this.QTcf.TabIndex = 11;
            // 
            // QTdata
            // 
            this.QTdata.Location = new System.Drawing.Point(43, 85);
            this.QTdata.Name = "QTdata";
            this.QTdata.Size = new System.Drawing.Size(177, 20);
            this.QTdata.TabIndex = 10;
            this.QTdata.ValueChanged += new System.EventHandler(this.QTdata_ValueChanged);
            // 
            // QTdescr
            // 
            this.QTdescr.Location = new System.Drawing.Point(43, 174);
            this.QTdescr.Name = "QTdescr";
            this.QTdescr.Size = new System.Drawing.Size(177, 20);
            this.QTdescr.TabIndex = 7;
            // 
            // QTimporto
            // 
            this.QTimporto.Location = new System.Drawing.Point(43, 128);
            this.QTimporto.Multiline = true;
            this.QTimporto.Name = "QTimporto";
            this.QTimporto.Size = new System.Drawing.Size(177, 20);
            this.QTimporto.TabIndex = 6;
            // 
            // QTnumFattura
            // 
            this.QTnumFattura.Location = new System.Drawing.Point(43, 36);
            this.QTnumFattura.Multiline = true;
            this.QTnumFattura.Name = "QTnumFattura";
            this.QTnumFattura.Size = new System.Drawing.Size(177, 20);
            this.QTnumFattura.TabIndex = 5;
            // 
            // SPnumFattura
            // 
            this.SPnumFattura.Location = new System.Drawing.Point(31, 36);
            this.SPnumFattura.Multiline = true;
            this.SPnumFattura.Name = "SPnumFattura";
            this.SPnumFattura.Size = new System.Drawing.Size(176, 20);
            this.SPnumFattura.TabIndex = 5;
            // 
            // SPImporto
            // 
            this.SPImporto.Location = new System.Drawing.Point(31, 128);
            this.SPImporto.Multiline = true;
            this.SPImporto.Name = "SPImporto";
            this.SPImporto.Size = new System.Drawing.Size(176, 20);
            this.SPImporto.TabIndex = 6;
            // 
            // SPdescr
            // 
            this.SPdescr.Location = new System.Drawing.Point(31, 174);
            this.SPdescr.Name = "SPdescr";
            this.SPdescr.Size = new System.Drawing.Size(176, 20);
            this.SPdescr.TabIndex = 7;
            // 
            // SPData
            // 
            this.SPData.Location = new System.Drawing.Point(31, 85);
            this.SPData.Name = "SPData";
            this.SPData.Size = new System.Drawing.Size(176, 20);
            this.SPData.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.SPp_iva);
            this.groupBox3.Controls.Add(this.SPinsert);
            this.groupBox3.Controls.Add(this.SPData);
            this.groupBox3.Controls.Add(this.SPdescr);
            this.groupBox3.Controls.Add(this.SPImporto);
            this.groupBox3.Controls.Add(this.SPnumFattura);
            this.groupBox3.Location = new System.Drawing.Point(558, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 295);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "SPONSORIZZAZIONE";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 201);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Partita IVA sponsor";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(28, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Numero Fattura";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(28, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Data";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Importo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Descrizione";
            // 
            // SPp_iva
            // 
            this.SPp_iva.Location = new System.Drawing.Point(31, 221);
            this.SPp_iva.Name = "SPp_iva";
            this.SPp_iva.Size = new System.Drawing.Size(176, 20);
            this.SPp_iva.TabIndex = 11;
            // 
            // SPinsert
            // 
            this.SPinsert.Location = new System.Drawing.Point(72, 257);
            this.SPinsert.Name = "SPinsert";
            this.SPinsert.Size = new System.Drawing.Size(75, 23);
            this.SPinsert.TabIndex = 10;
            this.SPinsert.Text = "INSERISCI";
            this.SPinsert.UseVisualStyleBackColor = true;
            this.SPinsert.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(23, 354);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 38);
            this.button2.TabIndex = 3;
            this.button2.Text = "Visualizza risultato";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(281, 309);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(507, 129);
            this.dataGridView1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "TESSERATI CHE NON HANNO PAGATO LA QUOTA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ANNO";
            // 
            // Anno
            // 
            this.Anno.Location = new System.Drawing.Point(136, 370);
            this.Anno.Name = "Anno";
            this.Anno.Size = new System.Drawing.Size(100, 20);
            this.Anno.TabIndex = 8;
            // 
            // Entrate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Anno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Entrate";
            this.Text = "Entrate";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox qsNumFattura;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox descrQS;
        private System.Windows.Forms.TextBox importoQS;
        private System.Windows.Forms.TextBox QTdescr;
        private System.Windows.Forms.TextBox QTimporto;
        private System.Windows.Forms.TextBox QTnumFattura;
        private System.Windows.Forms.DateTimePicker dataQuotaSociale;
        private System.Windows.Forms.DateTimePicker QTdata;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox SPnumFattura;
        private System.Windows.Forms.TextBox SPImporto;
        private System.Windows.Forms.TextBox SPdescr;
        private System.Windows.Forms.DateTimePicker SPData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button QTInsert;
        private System.Windows.Forms.TextBox QTcf;
        private System.Windows.Forms.Button SPinsert;
        private System.Windows.Forms.TextBox SPp_iva;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Anno;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
    }
}