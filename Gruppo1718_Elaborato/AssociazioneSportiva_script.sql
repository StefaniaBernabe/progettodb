CREATE DATABASE ASSOCIAZIONE_SPORTIVA;
GO
USE ASSOCIAZIONE_SPORTIVA;
GO
/****** Object:  Table [dbo].[acquisto_beni]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acquisto_beni](
	[codMateriale] [char](6) NOT NULL,
	[numFattura] [char](6) NOT NULL,
	[Data] [datetime] NOT NULL,
	[QuantitÃ] [float] NOT NULL,
 CONSTRAINT [IDacquisto_beni] PRIMARY KEY CLUSTERED 
(
	[codMateriale] ASC,
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACQUISTO_MATERIALE]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACQUISTO_MATERIALE](
	[numFattura] [char](6) NOT NULL,
	[Data] [datetime] NOT NULL,
	[Pag_Data] [date] NULL,
	[Pag_NumTransazione] [varchar](10) NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](40) NOT NULL,
	[Partita_Iva] [char](11) NOT NULL,
 CONSTRAINT [IDSPESA] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AFFITTO]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFITTO](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Pag_Data] [date] NULL,
	[Pag_NumTransazione] [varchar](10) NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](40) NOT NULL,
	[codImpianto] [char](2) NOT NULL,
	[num_campo] [numeric](1, 0) NOT NULL,
 CONSTRAINT [IDAFFITTO] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[allena]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allena](
	[codSquadra] [char](10) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDallena] PRIMARY KEY CLUSTERED 
(
	[codSquadra] ASC,
	[CF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ALLENAMENTO]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ALLENAMENTO](
	[codImpianto] [char](2) NOT NULL,
	[num_campo] [numeric](1, 0) NOT NULL,
	[Giorno] [varchar](15) NOT NULL,
	[Orario_inizio] [char](5) NOT NULL,
	[Orario_fine] [char](5) NOT NULL,
 CONSTRAINT [IDALLENAMENTO] PRIMARY KEY CLUSTERED 
(
	[codImpianto] ASC,
	[num_campo] ASC,
	[Giorno] ASC,
	[Orario_inizio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CARICA]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CARICA](
	[codCarica] [char](2) NOT NULL,
	[Nome] [varchar](20) NOT NULL,
	[Descrizione] [varchar](50) NOT NULL,
 CONSTRAINT [IDCARICA_ID] PRIMARY KEY CLUSTERED 
(
	[codCarica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CARICA_ANNUALE]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CARICA_ANNUALE](
	[CF] [char](16) NOT NULL,
	[Anno] [date] NOT NULL,
	[codCarica] [char](2) NOT NULL,
 CONSTRAINT [IDCARICA_ANNUALE] PRIMARY KEY CLUSTERED 
(
	[CF] ASC,
	[Anno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CENTRO_MEDICO_ASSOCIATO]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CENTRO_MEDICO_ASSOCIATO](
	[codCentroMedico] [char](2) NOT NULL,
	[RagioneSociale] [varchar](30) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Telefono] [numeric](10, 0) NOT NULL,
 CONSTRAINT [IDCENTRO_MEDICO_ASSOCIATO] PRIMARY KEY CLUSTERED 
(
	[codCentroMedico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[collabora]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[collabora](
	[codSquadra] [char](10) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDcollabora] PRIMARY KEY CLUSTERED 
(
	[codSquadra] ASC,
	[CF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMPENSO]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMPENSO](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Pag_Data] [date] NULL,
	[Pag_NumTransazione] [varchar](10) NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](40) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDCOMPENSO] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FORNITORE]    Script Date: 10/07/2019 09:26:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORNITORE](
	[Partita_Iva] [char](11) NOT NULL,
	[Ragione_Sociale] [varchar](30) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Iban] [varchar](27) NOT NULL,
 CONSTRAINT [IDFORNITORE] PRIMARY KEY CLUSTERED 
(
	[Partita_Iva] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[idoneità]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[idoneità](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[idoneo] [char](1) NOT NULL,
	[CF] [char](16) NOT NULL,
	[codCentroMedico] [char](2) NOT NULL,
 CONSTRAINT [FKido_VIS_ID] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IMPIANTO]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMPIANTO](
	[codImpianto] [char](2) NOT NULL,
	[num_campo] [numeric](1, 0) NOT NULL,
	[Nome] [varchar](30) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Spesa_annuale] [real] NOT NULL,
 CONSTRAINT [IDIMPIANTO] PRIMARY KEY CLUSTERED 
(
	[codImpianto] ASC,
	[num_campo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MATERIALE]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MATERIALE](
	[codMateriale] [char](6) NOT NULL,
	[Giacenza] [float] NOT NULL,
	[Descrizione] [varchar](30) NOT NULL,
 CONSTRAINT [IDMATERIALE] PRIMARY KEY CLUSTERED 
(
	[codMateriale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PAGAMENTO]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAGAMENTO](
	[Data] [date] NOT NULL,
	[NumTransazione] [varchar](10) NOT NULL,
	[ModalitÃ] [varchar](20) NOT NULL,
	[CodiceBanca] [varchar](10) NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDPAGAMENTO_ID] PRIMARY KEY CLUSTERED 
(
	[Data] ASC,
	[NumTransazione] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[partecipa]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[partecipa](
	[codSquadra] [char](10) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDpartecipa] PRIMARY KEY CLUSTERED 
(
	[CF] ASC,
	[codSquadra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PARTITA]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PARTITA](
	[codSquadra] [char](10) NOT NULL,
	[Data] [date] NOT NULL,
	[Numero_gara] [numeric](5, 0) NOT NULL,
	[Squadra_avversaria] [varchar](30) NOT NULL,
	[In_casa] [char](1) NOT NULL,
	[Punteggio_casa] [numeric](3, 0) NOT NULL,
	[Punteggio_ospite] [numeric](3, 0) NOT NULL,
 CONSTRAINT [IDPARTITA] PRIMARY KEY CLUSTERED 
(
	[codSquadra] ASC,
	[Data] ASC,
	[Numero_gara] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prevede]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[prevede](
	[codImpianto] [char](2) NOT NULL,
	[num_campo] [numeric](1, 0) NOT NULL,
	[Giorno] [varchar](15) NOT NULL,
	[Orario_inizio] [char](5) NOT NULL,
	[codSquadra] [char](10) NOT NULL,
 CONSTRAINT [IDprevede] PRIMARY KEY CLUSTERED 
(
	[codImpianto] ASC,
	[num_campo] ASC,
	[Giorno] ASC,
	[Orario_inizio] ASC,
	[codSquadra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QUOTA_ANNUALE_TESSERATO]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QUOTA_ANNUALE_TESSERATO](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](30) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDQUOTA_ANNUALE_TESSERATO] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QUOTA_SOCIALE]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QUOTA_SOCIALE](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](30) NOT NULL,
	[CF] [char](16) NOT NULL,
 CONSTRAINT [IDQUOTA_SOCIALE] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SOCIO]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SOCIO](
	[CF] [char](16) NOT NULL,
	[Nome] [varchar](20) NOT NULL,
	[Cognome] [varchar](20) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Telefono] [numeric](10, 0) NOT NULL,
	[Data_di_nascita] [date] NOT NULL,
	[Data_annessione] [date] NOT NULL,
 CONSTRAINT [IDSOCIO] PRIMARY KEY CLUSTERED 
(
	[CF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SPESA_DI_SPONSORIZZAZIONE]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SPESA_DI_SPONSORIZZAZIONE](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Pag_Data] [date] NULL,
	[Pag_NumTransazione] [varchar](10) NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](40) NOT NULL,
	[P_IVA] [char](11) NOT NULL,
 CONSTRAINT [IDSPESA_DI_SPNSORIZZAZIONE] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SPONSOR]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SPONSOR](
	[P_IVA] [char](11) NOT NULL,
	[Ragione_sociale] [varchar](30) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Telefono] [numeric](10, 0) NOT NULL,
 CONSTRAINT [IDSPONSOR] PRIMARY KEY CLUSTERED 
(
	[P_IVA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SPONSORIZZAZIONE]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SPONSORIZZAZIONE](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](30) NOT NULL,
	[P_IVA] [char](11) NOT NULL,
 CONSTRAINT [IDSPONSORIZZAZIONE] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SQUADRA]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SQUADRA](
	[codSquadra] [char](10) NOT NULL,
	[Nome] [varchar](30) NOT NULL,
	[Ore_di_allenamento_settimanali] [real] NOT NULL,
	[Giorni_di_allenamento] [numeric](1, 0) NOT NULL,
 CONSTRAINT [IDSQUADRA] PRIMARY KEY CLUSTERED 
(
	[codSquadra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TESSERATO]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TESSERATO](
	[CF] [char](16) NOT NULL,
	[Nome] [varchar](20) NOT NULL,
	[Cognome] [varchar](20) NOT NULL,
	[Indirizzo] [varchar](50) NOT NULL,
	[Telefono] [numeric](10, 0) NOT NULL,
	[Data_di_nascita] [date] NOT NULL,
	[Qualifica] [varchar](20) NOT NULL,
	[ScadenzaTesseramento] [date] NOT NULL,
 CONSTRAINT [IDTESSERATO] PRIMARY KEY CLUSTERED 
(
	[CF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[visita]    Script Date: 10/07/2019 09:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[visita](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[CF] [char](16) NOT NULL,
	[codCentroMedico] [char](2) NOT NULL,
 CONSTRAINT [FKvis_VIS_ID] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VISITA_MEDICA]    Script Date: 10/07/2019 09:26:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VISITA_MEDICA](
	[numFattura] [char](6) NOT NULL,
	[Data] [date] NOT NULL,
	[Pag_Data] [date] NULL,
	[Pag_NumTransazione] [varchar](10) NULL,
	[Importo] [real] NOT NULL,
	[Descrizione] [varchar](40) NOT NULL,
	[Tipo] [nvarchar](15) NOT NULL,
 CONSTRAINT [IDSPESA_ID] PRIMARY KEY CLUSTERED 
(
	[numFattura] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[acquisto_beni] ([codMateriale], [numFattura], [Data], [QuantitÃ]) VALUES (N'F12314', N'14788 ', CAST(N'2019-07-07T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[acquisto_beni] ([codMateriale], [numFattura], [Data], [QuantitÃ]) VALUES (N'M10000', N'000011', CAST(N'2019-06-22T00:00:00.000' AS DateTime), 400)
INSERT [dbo].[acquisto_beni] ([codMateriale], [numFattura], [Data], [QuantitÃ]) VALUES (N'M10000', N'122110', CAST(N'2019-07-07T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[ACQUISTO_MATERIALE] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Partita_Iva]) VALUES (N'000011', CAST(N'2019-06-22T00:00:00.000' AS DateTime), CAST(N'2019-07-07' AS Date), N'1223', 300, N'acquisto magliette squadra', N'14752233366')
INSERT [dbo].[ACQUISTO_MATERIALE] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Partita_Iva]) VALUES (N'122   ', CAST(N'2018-07-04T00:00:00.000' AS DateTime), NULL, NULL, 100, N'A', N'14752233366')
INSERT [dbo].[ACQUISTO_MATERIALE] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Partita_Iva]) VALUES (N'122110', CAST(N'2019-07-07T00:00:00.000' AS DateTime), NULL, NULL, 10, N'M', N'14752233366')
INSERT [dbo].[ACQUISTO_MATERIALE] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Partita_Iva]) VALUES (N'14788 ', CAST(N'2019-07-07T00:00:00.000' AS DateTime), NULL, NULL, 95, N'L', N'14687523014')
INSERT [dbo].[allena] ([codSquadra], [CF]) VALUES (N'0001      ', N'BRDMHL75L60L378L')
INSERT [dbo].[allena] ([codSquadra], [CF]) VALUES (N'0002      ', N'BSSVIO63C20D704J')
INSERT [dbo].[allena] ([codSquadra], [CF]) VALUES (N'0003      ', N'BRDMHL75L60L378L')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'GIOVEDÌ', N'10:00', N'12:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'GIOVEDÌ', N'18:00', N'20:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'LUNEDÌ', N'16:00', N'18:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'VENERDÌ', N'10:00', N'12:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'VENERDÌ', N'14:00', N'16:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'B2', CAST(1 AS Numeric(1, 0)), N'LUNEDÌ', N'17:00', N'19:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'B2', CAST(2 AS Numeric(1, 0)), N'LUNEDÌ', N'16:00', N'18:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'B2', CAST(2 AS Numeric(1, 0)), N'LUNEDÌ', N'18:00', N'20:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'B2', CAST(2 AS Numeric(1, 0)), N'MERCOLEDÌ', N'18:00', N'20:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'C7', CAST(1 AS Numeric(1, 0)), N'MARTEDÌ', N'15:00', N'17:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'C7', CAST(1 AS Numeric(1, 0)), N'MERCOLEDÌ', N'16:00', N'18:00')
INSERT [dbo].[ALLENAMENTO] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [Orario_fine]) VALUES (N'C7', CAST(1 AS Numeric(1, 0)), N'MERCOLEDÌ', N'18:00', N'20:00')
INSERT [dbo].[CARICA] ([codCarica], [Nome], [Descrizione]) VALUES (N'90', N'PRESIDENTE', N'PRESIDENTE DEL CONSIGLIO DEI SOCI')
INSERT [dbo].[CARICA] ([codCarica], [Nome], [Descrizione]) VALUES (N'91', N'TESORIERE', N'MANITENE LA CONTABILITA'' DELL''ASSOCIAZIONE')
INSERT [dbo].[CARICA] ([codCarica], [Nome], [Descrizione]) VALUES (N'92', N'VICEPRESIDENTE', N'SOSTITUISCE IL PRESIDENTE IN CASO DI ASSENZA')
INSERT [dbo].[CARICA] ([codCarica], [Nome], [Descrizione]) VALUES (N'93', N'SOCIO ONORARIO', N'SOCIO DELL''ASSOCIAZIONE')
INSERT [dbo].[CARICA_ANNUALE] ([CF], [Anno], [codCarica]) VALUES (N'MRNPRI53A11G478P', CAST(N'2000-02-03' AS Date), N'90')
INSERT [dbo].[CARICA_ANNUALE] ([CF], [Anno], [codCarica]) VALUES (N'MRNPRI53A11G478P', CAST(N'2014-10-12' AS Date), N'90')
INSERT [dbo].[CARICA_ANNUALE] ([CF], [Anno], [codCarica]) VALUES (N'RLOSDR77P10D704A', CAST(N'2000-02-03' AS Date), N'93')
INSERT [dbo].[CARICA_ANNUALE] ([CF], [Anno], [codCarica]) VALUES (N'RLOSDR77P10D704A', CAST(N'2014-10-12' AS Date), N'92')
INSERT [dbo].[CARICA_ANNUALE] ([CF], [Anno], [codCarica]) VALUES (N'RTLSRN82D15H199X', CAST(N'2015-12-20' AS Date), N'91')
INSERT [dbo].[CENTRO_MEDICO_ASSOCIATO] ([codCentroMedico], [RagioneSociale], [Indirizzo], [Telefono]) VALUES (N'C1', N'OSPEDALE MORGAGNI', N'VIALE RISORGIMENTO 33, FORLI''', CAST(3412547894 AS Numeric(10, 0)))
INSERT [dbo].[CENTRO_MEDICO_ASSOCIATO] ([codCentroMedico], [RagioneSociale], [Indirizzo], [Telefono]) VALUES (N'C2', N'VILLA SMERALDA', N'VIA TORINO 2, BOLOGNA', CAST(2014525578 AS Numeric(10, 0)))
INSERT [dbo].[collabora] ([codSquadra], [CF]) VALUES (N'0001      ', N'NRELSN62T07C573D')
INSERT [dbo].[collabora] ([codSquadra], [CF]) VALUES (N'0002      ', N'RSSMRA80A01L781K')
INSERT [dbo].[FORNITORE] ([Partita_Iva], [Ragione_Sociale], [Indirizzo], [Iban]) VALUES (N'14687523014', N'Adidas', N'Via cobalto 11, Roma', N'IT12T4789654102354789412333')
INSERT [dbo].[FORNITORE] ([Partita_Iva], [Ragione_Sociale], [Indirizzo], [Iban]) VALUES (N'14752233366', N'Nike', N'Via Roma 44, Bologna', N'IT12T1452358741256555884100')
INSERT [dbo].[IMPIANTO] ([codImpianto], [num_campo], [Nome], [Indirizzo], [Spesa_annuale]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'GINNASIO SPORTIVO', N'VIALE DELLA LIBERTA'' 13', 750)
INSERT [dbo].[IMPIANTO] ([codImpianto], [num_campo], [Nome], [Indirizzo], [Spesa_annuale]) VALUES (N'B2', CAST(1 AS Numeric(1, 0)), N'PALESTRA G.MONTI', N'VIA DELLE NAZIONI 22', 400)
INSERT [dbo].[IMPIANTO] ([codImpianto], [num_campo], [Nome], [Indirizzo], [Spesa_annuale]) VALUES (N'B2', CAST(2 AS Numeric(1, 0)), N'PALESTRA G.MONTI', N'VIA DELLE NAZIONI 22', 400)
INSERT [dbo].[IMPIANTO] ([codImpianto], [num_campo], [Nome], [Indirizzo], [Spesa_annuale]) VALUES (N'C7', CAST(1 AS Numeric(1, 0)), N'PALESTRA G.BERSANI', N'VIA ORCEOLI 10', 350)
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'A12445', 10, N'A')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'F12314', 100, N'P')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'M10000', 410, N'Magliette squadra')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'M11000', 10, N'palloni')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'M11100', 10, N'p')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'M12345', 30, N'Birilli')
INSERT [dbo].[MATERIALE] ([codMateriale], [Giacenza], [Descrizione]) VALUES (N'M20000', 25, N'Palloni')
INSERT [dbo].[PAGAMENTO] ([Data], [NumTransazione], [ModalitÃ], [CodiceBanca], [CF]) VALUES (CAST(N'2019-07-07' AS Date), N'1223', N'Contanti', NULL, N'MRCLGU60B07F097U')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0002      ', N'BLDSFO99H56H199L')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0001      ', N'CSTDNL97A31D548M')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0002      ', N'GNNMNC87L20D704O')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0001      ', N'GNTLSN90M07F205B')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0003      ', N'MNTSRA05E55D704D')
INSERT [dbo].[partecipa] ([codSquadra], [CF]) VALUES (N'0002      ', N'MRLGRC96L54B354S')
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0001      ', CAST(N'2019-03-12' AS Date), CAST(3 AS Numeric(5, 0)), N'BOLOGNA', N'1', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0001      ', CAST(N'2019-03-27' AS Date), CAST(5 AS Numeric(5, 0)), N'AVELLINO', N'0', CAST(0 AS Numeric(3, 0)), CAST(3 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0001      ', CAST(N'2019-04-15' AS Date), CAST(1 AS Numeric(5, 0)), N'FORLÌ', N'1', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0002      ', CAST(N'2019-07-05' AS Date), CAST(1 AS Numeric(5, 0)), N'FORLI''', N'1', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0002      ', CAST(N'2019-07-11' AS Date), CAST(2 AS Numeric(5, 0)), N'PARMA', N'1', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0003      ', CAST(N'2019-07-10' AS Date), CAST(2 AS Numeric(5, 0)), N'PESCARA', N'0', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0003      ', CAST(N'2019-07-11' AS Date), CAST(1 AS Numeric(5, 0)), N'PIACENZA', N'0', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0004      ', CAST(N'2019-05-20' AS Date), CAST(1 AS Numeric(5, 0)), N'FORLÌ', N'1', CAST(0 AS Numeric(3, 0)), CAST(3 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0005      ', CAST(N'2019-06-01' AS Date), CAST(2 AS Numeric(5, 0)), N'CERVIA', N'0', CAST(0 AS Numeric(3, 0)), CAST(3 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0006      ', CAST(N'2019-06-01' AS Date), CAST(3 AS Numeric(5, 0)), N'BOLOGNA', N'1', CAST(3 AS Numeric(3, 0)), CAST(0 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0006      ', CAST(N'2019-06-25' AS Date), CAST(2 AS Numeric(5, 0)), N'MILANO', N'0', CAST(1 AS Numeric(3, 0)), CAST(2 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0007      ', CAST(N'2019-05-25' AS Date), CAST(2 AS Numeric(5, 0)), N'RAVENNA', N'1', CAST(2 AS Numeric(3, 0)), CAST(1 AS Numeric(3, 0)))
INSERT [dbo].[PARTITA] ([codSquadra], [Data], [Numero_gara], [Squadra_avversaria], [In_casa], [Punteggio_casa], [Punteggio_ospite]) VALUES (N'0007      ', CAST(N'2019-05-30' AS Date), CAST(1 AS Numeric(5, 0)), N'CERVIA', N'1', CAST(3 AS Numeric(3, 0)), CAST(0 AS Numeric(3, 0)))
INSERT [dbo].[prevede] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [codSquadra]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'GIOVEDÌ', N'10:00', N'0001      ')
INSERT [dbo].[prevede] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [codSquadra]) VALUES (N'A1', CAST(1 AS Numeric(1, 0)), N'GIOVEDÌ', N'18:00', N'0002      ')
INSERT [dbo].[prevede] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [codSquadra]) VALUES (N'B2', CAST(1 AS Numeric(1, 0)), N'LUNEDÌ', N'17:00', N'0003      ')
INSERT [dbo].[prevede] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [codSquadra]) VALUES (N'B2', CAST(2 AS Numeric(1, 0)), N'LUNEDÌ', N'16:00', N'0001      ')
INSERT [dbo].[prevede] ([codImpianto], [num_campo], [Giorno], [Orario_inizio], [codSquadra]) VALUES (N'C7', CAST(1 AS Numeric(1, 0)), N'MERCOLEDÌ', N'18:00', N'0001      ')
INSERT [dbo].[QUOTA_ANNUALE_TESSERATO] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'10012 ', CAST(N'2018-09-13' AS Date), 150, N'QUOTA COLLABORATORE', N'MNNDVD78R23D704I')
INSERT [dbo].[QUOTA_ANNUALE_TESSERATO] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'10243 ', CAST(N'2017-09-12' AS Date), 200, N'QUOTA TECNICO', N'BRDMHL75L60L378L')
INSERT [dbo].[QUOTA_ANNUALE_TESSERATO] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'10395 ', CAST(N'2018-10-01' AS Date), 300, N'QUOTA UNDER', N'MNTSRA05E55D704D')
INSERT [dbo].[QUOTA_SOCIALE] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'103495', CAST(N'2008-09-20' AS Date), 150, N'QUOTA SOCIALE DI LUIGI MARCHI', N'MRCLGU60B07F097U')
INSERT [dbo].[QUOTA_SOCIALE] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'110944', CAST(N'2017-10-13' AS Date), 150, N'QUOTA SOCIALE DI SANDRO ROLI', N'RLOSDR77P10D704A')
INSERT [dbo].[QUOTA_SOCIALE] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'114059', CAST(N'2018-09-28' AS Date), 150, N'QUOTA SOCIALE ORTOLANI', N'RTLSRN82D15H199X')
INSERT [dbo].[QUOTA_SOCIALE] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'114060', CAST(N'2018-09-28' AS Date), 150, N'QUOTA SOCIALE ROLI', N'RLOSDR77P10D704A')
INSERT [dbo].[QUOTA_SOCIALE] ([numFattura], [Data], [Importo], [Descrizione], [CF]) VALUES (N'1245  ', CAST(N'2019-07-04' AS Date), 200, N'QUOTA SOCIALE MARCHI', N'MRNPRI53A11G478P')
INSERT [dbo].[SOCIO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Data_annessione]) VALUES (N'MRCLGU60B07F097U', N'LUIGI', N'MARCHI', N'VIA FAENZA 23', CAST(3339988777 AS Numeric(10, 0)), CAST(N'1960-02-07' AS Date), CAST(N'2004-10-12' AS Date))
INSERT [dbo].[SOCIO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Data_annessione]) VALUES (N'MRNPRI53A11G478P', N'PIERO', N'MARINI', N'VIA PETRARCA 67', CAST(3293884620 AS Numeric(10, 0)), CAST(N'1953-01-11' AS Date), CAST(N'1998-03-12' AS Date))
INSERT [dbo].[SOCIO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Data_annessione]) VALUES (N'RLOSDR77P10D704A', N'SANDRO', N'ROLI', N'VIA VERDI 9', CAST(3277465739 AS Numeric(10, 0)), CAST(N'1977-09-10' AS Date), CAST(N'2013-09-04' AS Date))
INSERT [dbo].[SOCIO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Data_annessione]) VALUES (N'RTLSRN82D15H199X', N'SERENA', N'ORTOLANI', N'VIA TRIPOLI 39', CAST(3244857337 AS Numeric(10, 0)), CAST(N'1982-04-15' AS Date), CAST(N'2017-11-15' AS Date))
INSERT [dbo].[SPONSOR] ([P_IVA], [Ragione_sociale], [Indirizzo], [Telefono]) VALUES (N'15974230185', N'SPORTSERVICE', N'VIA DELLA LIBERTA'' 35, FORLI''', CAST(3459471105 AS Numeric(10, 0)))
INSERT [dbo].[SPONSOR] ([P_IVA], [Ragione_sociale], [Indirizzo], [Telefono]) VALUES (N'24587621479', N'ADIDAS', N'VIA MILANO 2, BOLOGNA', CAST(3512647821 AS Numeric(10, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0001      ', N'Serie B maschile', 10, CAST(5 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0002      ', N'Serie B femminile', 8, CAST(4 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0003      ', N'UNDER 16 FEMMINILE', 6, CAST(3 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0004      ', N'Seria A maschile', 10, CAST(5 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0005      ', N'Serie A femminile', 10, CAST(5 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0006      ', N'Serie C maschile', 5, CAST(3 AS Numeric(1, 0)))
INSERT [dbo].[SQUADRA] ([codSquadra], [Nome], [Ore_di_allenamento_settimanali], [Giorni_di_allenamento]) VALUES (N'0007      ', N'Serie D femminile', 4, CAST(2 AS Numeric(1, 0)))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'BLDSFO99H56H199L', N'SOFIA', N'BALDUCCI', N'VIA RAVEGNANA 239', CAST(3372647109 AS Numeric(10, 0)), CAST(N'1999-06-16' AS Date), N'ATLETA', CAST(N'2019-10-12' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'BRDMHL75L60L378L', N'MICHELA', N'BRIDI', N'VIA VITTORIO VENETO 5', CAST(3844756200 AS Numeric(10, 0)), CAST(N'1975-07-20' AS Date), N'TECNICO', CAST(N'2019-11-03' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'BSSVIO63C20D704J', N'IVO', N'BASSI', N'VIA DEI TULIPANI 20', CAST(3304998129 AS Numeric(10, 0)), CAST(N'1963-03-20' AS Date), N'TECNICO', CAST(N'2018-04-12' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'CSTDNL97A31D548M', N'DANIELE', N'COSTANZELLI', N'VIA ALIGHIERI 33', CAST(3334657880 AS Numeric(10, 0)), CAST(N'1997-01-31' AS Date), N'ATLETA', CAST(N'2019-09-15' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'GNNMNC87L20D704O', N'MONICA', N'GENNARO', N'VIA NAPOLI 6', CAST(3992736456 AS Numeric(10, 0)), CAST(N'1987-07-20' AS Date), N'ATLETA', CAST(N'2018-10-19' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'GNTLSN90M07F205B', N'ALESSANDRO', N'GENTILE', N'VIA TRIESTE 11', CAST(3277463589 AS Numeric(10, 0)), CAST(N'1990-08-07' AS Date), N'ATLETA', CAST(N'2019-09-29' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'MNNDVD78R23D704I', N'DAVIDE', N'MANNINO', N'VIA FIRENZE 1', CAST(3388456722 AS Numeric(10, 0)), CAST(N'1978-10-23' AS Date), N'COLLABORATORE', CAST(N'2019-12-10' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'MNTSRA05E55D704D', N'SARA', N'MONTI', N'VIA ULPIANO 3', CAST(3332244955 AS Numeric(10, 0)), CAST(N'2005-05-15' AS Date), N'ATLETA', CAST(N'2019-09-01' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'MRLGRC96L54B354S', N'GRECA', N'MORELLI', N'VIA DEL CORSO 7', CAST(3275847987 AS Numeric(10, 0)), CAST(N'1996-07-14' AS Date), N'ATLETA', CAST(N'2018-09-12' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'NRELSN62T07C573D', N'ALESSANDRO', N'NERI', N'VIA DEI MILLE 1000', CAST(3457320937 AS Numeric(10, 0)), CAST(N'1962-12-07' AS Date), N'COLLABORATORE', CAST(N'2018-10-07' AS Date))
INSERT [dbo].[TESSERATO] ([CF], [Nome], [Cognome], [Indirizzo], [Telefono], [Data_di_nascita], [Qualifica], [ScadenzaTesseramento]) VALUES (N'RSSMRA80A01L781K', N'MARIO', N'ROSSI', N'CORSO MAZZINI 12', CAST(3334859627 AS Numeric(10, 0)), CAST(N'1980-01-01' AS Date), N'COLLABORATORE', CAST(N'2019-10-03' AS Date))
INSERT [dbo].[visita] ([numFattura], [Data], [CF], [codCentroMedico]) VALUES (N'12    ', CAST(N'2018-05-17' AS Date), N'GNNMNC87L20D704O', N'C1')
INSERT [dbo].[visita] ([numFattura], [Data], [CF], [codCentroMedico]) VALUES (N'35    ', CAST(N'2018-06-27' AS Date), N'GNNMNC87L20D704O', N'C2')
INSERT [dbo].[visita] ([numFattura], [Data], [CF], [codCentroMedico]) VALUES (N'74    ', CAST(N'2018-09-01' AS Date), N'MNNDVD78R23D704I', N'C1')
INSERT [dbo].[visita] ([numFattura], [Data], [CF], [codCentroMedico]) VALUES (N'95    ', CAST(N'2018-10-28' AS Date), N'MRLGRC96L54B354S', N'C2')
INSERT [dbo].[VISITA_MEDICA] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Tipo]) VALUES (N'12    ', CAST(N'2018-05-17' AS Date), CAST(N'2018-09-01' AS Date), N'100214', 50, N'VISITA INFORTUNIO SPALLA', N'INFORTUNIO')
INSERT [dbo].[VISITA_MEDICA] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Tipo]) VALUES (N'35    ', CAST(N'2018-06-27' AS Date), NULL, NULL, 70, N'VISITA INFORTUNIO PIEDE', N'INFORTUNIO')
INSERT [dbo].[VISITA_MEDICA] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Tipo]) VALUES (N'74    ', CAST(N'2018-09-01' AS Date), CAST(N'2018-09-01' AS Date), N'123459', 45, N'VISITA INFORTUNIO SPALLA', N'INFORTUNIO')
INSERT [dbo].[VISITA_MEDICA] ([numFattura], [Data], [Pag_Data], [Pag_NumTransazione], [Importo], [Descrizione], [Tipo]) VALUES (N'95    ', CAST(N'2018-10-28' AS Date), NULL, NULL, 25, N'VISITA MEDICA', N'VISITA MEDICA')
ALTER TABLE [dbo].[acquisto_beni]  WITH CHECK ADD  CONSTRAINT [FKacq_ACQ] FOREIGN KEY([numFattura], [Data])
REFERENCES [dbo].[ACQUISTO_MATERIALE] ([numFattura], [Data])
GO
ALTER TABLE [dbo].[acquisto_beni] CHECK CONSTRAINT [FKacq_ACQ]
GO
ALTER TABLE [dbo].[acquisto_beni]  WITH CHECK ADD  CONSTRAINT [FKacq_MAT] FOREIGN KEY([codMateriale])
REFERENCES [dbo].[MATERIALE] ([codMateriale])
GO
ALTER TABLE [dbo].[acquisto_beni] CHECK CONSTRAINT [FKacq_MAT]
GO
ALTER TABLE [dbo].[ACQUISTO_MATERIALE]  WITH CHECK ADD  CONSTRAINT [FKcompetenza_cliente] FOREIGN KEY([Partita_Iva])
REFERENCES [dbo].[FORNITORE] ([Partita_Iva])
GO
ALTER TABLE [dbo].[ACQUISTO_MATERIALE] CHECK CONSTRAINT [FKcompetenza_cliente]
GO
ALTER TABLE [dbo].[AFFITTO]  WITH CHECK ADD  CONSTRAINT [FKaffittare] FOREIGN KEY([codImpianto], [num_campo])
REFERENCES [dbo].[IMPIANTO] ([codImpianto], [num_campo])
GO
ALTER TABLE [dbo].[AFFITTO] CHECK CONSTRAINT [FKaffittare]
GO
ALTER TABLE [dbo].[AFFITTO]  WITH CHECK ADD  CONSTRAINT [FKpagamento_affitto_FK] FOREIGN KEY([Pag_Data], [Pag_NumTransazione])
REFERENCES [dbo].[PAGAMENTO] ([Data], [NumTransazione])
GO
ALTER TABLE [dbo].[AFFITTO] CHECK CONSTRAINT [FKpagamento_affitto_FK]
GO
ALTER TABLE [dbo].[allena]  WITH CHECK ADD  CONSTRAINT [FKall_SQU] FOREIGN KEY([codSquadra])
REFERENCES [dbo].[SQUADRA] ([codSquadra])
GO
ALTER TABLE [dbo].[allena] CHECK CONSTRAINT [FKall_SQU]
GO
ALTER TABLE [dbo].[allena]  WITH CHECK ADD  CONSTRAINT [FKall_TES] FOREIGN KEY([CF])
REFERENCES [dbo].[TESSERATO] ([CF])
GO
ALTER TABLE [dbo].[allena] CHECK CONSTRAINT [FKall_TES]
GO
ALTER TABLE [dbo].[ALLENAMENTO]  WITH CHECK ADD  CONSTRAINT [FKluogo] FOREIGN KEY([codImpianto], [num_campo])
REFERENCES [dbo].[IMPIANTO] ([codImpianto], [num_campo])
GO
ALTER TABLE [dbo].[ALLENAMENTO] CHECK CONSTRAINT [FKluogo]
GO
ALTER TABLE [dbo].[CARICA_ANNUALE]  WITH CHECK ADD  CONSTRAINT [FKdetiene] FOREIGN KEY([CF])
REFERENCES [dbo].[SOCIO] ([CF])
GO
ALTER TABLE [dbo].[CARICA_ANNUALE] CHECK CONSTRAINT [FKdetiene]
GO
ALTER TABLE [dbo].[CARICA_ANNUALE]  WITH CHECK ADD  CONSTRAINT [FKstorico] FOREIGN KEY([codCarica])
REFERENCES [dbo].[CARICA] ([codCarica])
GO
ALTER TABLE [dbo].[CARICA_ANNUALE] CHECK CONSTRAINT [FKstorico]
GO
ALTER TABLE [dbo].[collabora]  WITH CHECK ADD  CONSTRAINT [FKcol_SQU] FOREIGN KEY([codSquadra])
REFERENCES [dbo].[SQUADRA] ([codSquadra])
GO
ALTER TABLE [dbo].[collabora] CHECK CONSTRAINT [FKcol_SQU]
GO
ALTER TABLE [dbo].[collabora]  WITH CHECK ADD  CONSTRAINT [FKcol_TES] FOREIGN KEY([CF])
REFERENCES [dbo].[TESSERATO] ([CF])
GO
ALTER TABLE [dbo].[collabora] CHECK CONSTRAINT [FKcol_TES]
GO
ALTER TABLE [dbo].[COMPENSO]  WITH CHECK ADD  CONSTRAINT [FKpagamento_compenso_FK] FOREIGN KEY([Pag_Data], [Pag_NumTransazione])
REFERENCES [dbo].[PAGAMENTO] ([Data], [NumTransazione])
GO
ALTER TABLE [dbo].[COMPENSO] CHECK CONSTRAINT [FKpagamento_compenso_FK]
GO
ALTER TABLE [dbo].[COMPENSO]  WITH CHECK ADD  CONSTRAINT [FKriceve] FOREIGN KEY([CF])
REFERENCES [dbo].[TESSERATO] ([CF])
GO
ALTER TABLE [dbo].[COMPENSO] CHECK CONSTRAINT [FKriceve]
GO
ALTER TABLE [dbo].[idoneità]  WITH CHECK ADD  CONSTRAINT [FKido_TES] FOREIGN KEY([CF])
REFERENCES [dbo].[TESSERATO] ([CF])
GO
ALTER TABLE [dbo].[idoneità] CHECK CONSTRAINT [FKido_TES]
GO
ALTER TABLE [dbo].[idoneità]  WITH CHECK ADD  CONSTRAINT [FKido_VIS_FK] FOREIGN KEY([numFattura], [Data])
REFERENCES [dbo].[VISITA_MEDICA] ([numFattura], [Data])
GO
ALTER TABLE [dbo].[idoneità] CHECK CONSTRAINT [FKido_VIS_FK]
GO
ALTER TABLE [dbo].[QUOTA_ANNUALE_TESSERATO]  WITH CHECK ADD  CONSTRAINT [FKquota_tesserato] FOREIGN KEY([CF])
REFERENCES [dbo].[TESSERATO] ([CF])
GO
ALTER TABLE [dbo].[QUOTA_ANNUALE_TESSERATO] CHECK CONSTRAINT [FKquota_tesserato]
GO
ALTER TABLE [dbo].[QUOTA_SOCIALE]  WITH CHECK ADD  CONSTRAINT [FKquota_socio] FOREIGN KEY([CF])
REFERENCES [dbo].[SOCIO] ([CF])
GO
ALTER TABLE [dbo].[QUOTA_SOCIALE] CHECK CONSTRAINT [FKquota_socio]
GO
ALTER TABLE [dbo].[SPESA_DI_SPONSORIZZAZIONE]  WITH CHECK ADD  CONSTRAINT [FKpagamento_sponsorizzazione_FK] FOREIGN KEY([Pag_Data], [Pag_NumTransazione])
REFERENCES [dbo].[PAGAMENTO] ([Data], [NumTransazione])
GO
ALTER TABLE [dbo].[SPESA_DI_SPONSORIZZAZIONE] CHECK CONSTRAINT [FKpagamento_sponsorizzazione_FK]
GO
ALTER TABLE [dbo].[SPONSORIZZAZIONE]  WITH CHECK ADD  CONSTRAINT [FKquota_sponor] FOREIGN KEY([P_IVA])
REFERENCES [dbo].[SPONSOR] ([P_IVA])
GO
ALTER TABLE [dbo].[SPONSORIZZAZIONE] CHECK CONSTRAINT [FKquota_sponor]
GO
ALTER TABLE [dbo].[VISITA_MEDICA]  WITH NOCHECK ADD  CONSTRAINT [FKpagamento_visita_medica_FK] FOREIGN KEY([Pag_Data], [Pag_NumTransazione])
REFERENCES [dbo].[PAGAMENTO] ([Data], [NumTransazione])
GO
ALTER TABLE [dbo].[VISITA_MEDICA] CHECK CONSTRAINT [FKpagamento_visita_medica_FK]
GO
ALTER TABLE [dbo].[ACQUISTO_MATERIALE]  WITH CHECK ADD  CONSTRAINT [FKpagamento_materiale_CHK] CHECK  (([Pag_Data] IS NOT NULL AND [Pag_NumTransazione] IS NOT NULL OR [Pag_Data] IS NULL AND [Pag_NumTransazione] IS NULL))
GO
ALTER TABLE [dbo].[ACQUISTO_MATERIALE] CHECK CONSTRAINT [FKpagamento_materiale_CHK]
GO
ALTER TABLE [dbo].[AFFITTO]  WITH CHECK ADD  CONSTRAINT [FKpagamento_affitto_CHK] CHECK  (([Pag_Data] IS NOT NULL AND [Pag_NumTransazione] IS NOT NULL OR [Pag_Data] IS NULL AND [Pag_NumTransazione] IS NULL))
GO
ALTER TABLE [dbo].[AFFITTO] CHECK CONSTRAINT [FKpagamento_affitto_CHK]
GO
ALTER TABLE [dbo].[COMPENSO]  WITH CHECK ADD  CONSTRAINT [FKpagamento_compenso_CHK] CHECK  (([Pag_Data] IS NOT NULL AND [Pag_NumTransazione] IS NOT NULL OR [Pag_Data] IS NULL AND [Pag_NumTransazione] IS NULL))
GO
ALTER TABLE [dbo].[COMPENSO] CHECK CONSTRAINT [FKpagamento_compenso_CHK]
GO
ALTER TABLE [dbo].[SPESA_DI_SPONSORIZZAZIONE]  WITH CHECK ADD  CONSTRAINT [FKpagamento_sponsorizzazione_CHK] CHECK  (([Pag_Data] IS NOT NULL AND [Pag_NumTransazione] IS NOT NULL OR [Pag_Data] IS NULL AND [Pag_NumTransazione] IS NULL))
GO
ALTER TABLE [dbo].[SPESA_DI_SPONSORIZZAZIONE] CHECK CONSTRAINT [FKpagamento_sponsorizzazione_CHK]
GO
ALTER TABLE [dbo].[VISITA_MEDICA]  WITH NOCHECK ADD  CONSTRAINT [FKpagamento_visita_medica_CHK] CHECK  (([Pag_Data] IS NOT NULL AND [Pag_NumTransazione] IS NOT NULL OR [Pag_Data] IS NULL AND [Pag_NumTransazione] IS NULL))
GO
ALTER TABLE [dbo].[VISITA_MEDICA] CHECK CONSTRAINT [FKpagamento_visita_medica_CHK]
GO
